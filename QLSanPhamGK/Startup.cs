﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(QLSanPhamGK.Startup))]
namespace QLSanPhamGK
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
